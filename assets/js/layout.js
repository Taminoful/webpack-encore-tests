'use strict';

import $ from 'jquery';
import 'bootstrap';
import 'bootstrap-sass/assets/stylesheets/_bootstrap.scss';
import 'font-awesome/scss/font-awesome.scss';
import '../css/main.scss';

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

